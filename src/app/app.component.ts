import { Component } from '@angular/core';
import { MyserviceService } from './myservice.service'

@Component({
  selector: 'hlw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';
  todaydate:Date;

  constructor(private myservice:MyserviceService){
    this.todaydate = this.myservice.todayDate();
  }
  /*
  ngOnInit(){
    this.todaydate = this.myservice.todayDate();
  }
  */

}
