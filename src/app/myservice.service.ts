import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {
  //ceci est un service qui fournie la methode todayDate();
  //cette methode est utilisable dans tous les composants 
  //qui injectent ce service.
  todayDate():Date{
    let nDate = new Date();
    return nDate;
  }

  constructor() { }
}
