import { Component, OnInit } from '@angular/core';
import { RecupService } from '../recup.service';

@Component({
  selector: 'hlw-four-color',
  templateUrl: './four-color.component.html',
  styleUrls: ['./four-color.component.css']
})
export class FourColorComponent implements OnInit {
  tableauImgs = [];
  constructor(private recup: RecupService) {
    for (let i = 0; i < recup.getCours().length; i++) {
      this.tableauImgs[i] = {
        bool: true,
        url:recup.getCours()[i]["urlImage"]
      }
    }
  }
  onMouseOver(i){
    this.tableauImgs[i]["bool"] = true;
  }

  onMouseOut(i){
    this.tableauImgs[i]["bool"] = false;
  }

  ngOnInit() {

  }



  // afficherOptions(event){
  //   event.target.style.display= "none";
  // }
}