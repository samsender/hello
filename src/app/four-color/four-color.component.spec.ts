import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourColorComponent } from './four-color.component';

describe('FourColorComponent', () => {
  let component: FourColorComponent;
  let fixture: ComponentFixture<FourColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
