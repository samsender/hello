import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RecupService {
  cours = [
    {
      langage: "html",
      urlImage: "/assets/html5.png",
      cours: [
        {
          nomCours: "cours1",
          description: "text",
          urlPdf: "url"
        },
        {
          nomCours: "cours2",
          description: "text",
          urlPdf: "url"
        }
      ],
      exo: [
        {
          consigne: "url pdf",
          correction: "web page"
        },
        {
          consigne: "url pdf2",
          correction: "web pages 2"
        }
      ]
    },
    {
      langage: "CSS",
      urlImage: "/assets/css3.jpg",
      cours: [
        {
          nomCours: "cours1",
          description: "text",
          urlPdf: "url"
        },
        {
          nomCours: "cours2",
          description: "text",
          urlPdf: "url"
        }
      ],
      exo: [
        {
          consigne: "url pdf",
          correction: "web page"
        },
        {
          consigne: "pdf2",
          correction: "web pages 2"
        }
      ]
    },
    {
      langage: "JAVASCRIPT",
      urlImage: "/assets/js.jpg",
      cours: [
        {
          nomCours: "cours1",
          description: "text",
          urlPdf: "url"
        },
        {
          nomCours: "cours2",
          description: "text",
          urlPdf: "url"
        }
      ],
      exo: [
        {
          consigne: "url pdf",
          correction: "web page"
        },
        {
          consigne: "pdf2",
          correction: "web pages 2"
        }
      ]
    },
    {
      langage: "ANGULAR",
      urlImage: "/assets/angular.png",
      cours: [
        {
          nomCours: "cours1",
          description: "text",
          urlPdf: "url"
        },
        {
          nomCours: "cours2",
          description: "text",
          urlPdf: "url"
        }
      ],
      exo: [
        {
          consigne: "url pdf",
          correction: "web page"
        },
        {
          consigne: "pdf2",
          correction: "web pages 2"
        }
      ]
    }
  ]
  getCours():Array<any>{
    return this.cours; 
  }
  constructor() { }

}
