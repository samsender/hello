import { Component, OnInit, Input } from '@angular/core';
import { RecupService } from '../recup.service';

@Component({
  selector: 'hlw-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  hover :boolean = true;
  @Input() image:string;
  constructor(private recup: RecupService) {
  }
  ngOnInit() {
  }
}
