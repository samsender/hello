import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hlw-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit {
  numberOfBooks:number;
  bookImage:string;
  bookImageWidth:number;
  bookImageHeight:number;
  isHidden = false;
  constructor() {
    this.numberOfBooks = 77;
    this.bookImage = "/assets/css3.jpg"
    this.bookImageHeight = 300;
    this.bookImageWidth = 300;
   }

   onButtonClick():void{
     this.isHidden = !this.isHidden;
   }
   onBlueClick(event){
     event.target.parentNode.style.backgroundColor = "green";
   }
   onGreenClick(event){
    event.target.parentNode.parentNode.style.backgroundColor = "cyan";
   }

  ngOnInit() {
  }

}


